FROM node:12.18.4
WORKDIR /rest
COPY package*.json ./
RUN npm install
#RUN apt-get update
#RUN apt-get install -y nano net-tools vim
COPY . .
EXPOSE 3000
CMD ["npm", "start"]

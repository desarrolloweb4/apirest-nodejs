import express, { Application } from "express";
import morgan from "morgan";
import cors from "cors";
import http from 'http';
import io from 'socket.io';
//import io from 'socket.io';
import showBanner from 'node-banner';
//import eur, { Eureka } from 'eureka-js-client';

import userRoutes from './routes/userRoutes';
import messageRoutes from './routes/messageRoutes';
import publicacionRoutes from './routes/publicacionRoutes'

class Server {
  public app: Application;
  public httpapp: any;
  public sockets: any;
  //public eure: any;

  constructor() {
    this.app = express();
    this.httpapp = new http.Server(this.app);
    this.sockets = io.listen(this.httpapp);
    // this.eure =
    this.config();
    this.routes();
  }

  config(): void {
    this.app.set("port", process.env.PORT || 3000);
    this.app.use(morgan("dev"));
    this.app.use(cors());
    this.app.use(express.json());
    this.app.use(express.urlencoded({ extended: false }));
  }

  routes(): void{
    this.app.use("/api/user", userRoutes);
    this.app.use("/api/message", messageRoutes);
    this.app.use("/api/publicacion", publicacionRoutes)
  }

  start() {
    this.sockets.on('connection', (socket: io.Socket) => {
      console.log('usuario conectado');
      socket.on('message', (msg: string) => {
        console.log(msg);
        socket.broadcast.emit('messages', msg);
      });
      socket.on('disconnect', () => {
        console.log('usuario desconectado');
      });
    });
    this.httpapp.listen(this.app.get('port'), () =>{
      showBanner('API   REST   FROM   NODEJS', `Server on port ${this.app.get('port')}`);
      //console.log("Sever on port", this.app.get("port"));
    });
    // this.app.listen(this.app.get("port"), () => {
    //showBanner('Api   Rest   from   Nodejs', `Server on port ${this.app.get('port')}`);
    //     showBanner('API   REST   FROM   NODEJS', `Server on port ${this.app.get('port')}`);
    //console.log("Sever on port", this.app.get("port"));
    // });
  }

}

const serve = new Server();
serve.start();

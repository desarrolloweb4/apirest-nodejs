import { Request, Response } from "express";

import db from "../base/database";

class UserController {
  public async getAll(req: Request, res: Response) {
    try{
      const all = await db.query("SELECT * FROM user", (err, rows) => {
        if (!err) {
          res.setHeader('Content-Type', 'application/json');
          res.status(200).send(rows);
          console.log(JSON.stringify(rows));
        } else {
          res.status(500).send(err.sqlMessage);
          console.log(err);
        }
      });
    }catch(error){
      console.log(error);
    }
  }

  public async get(req: Request, res: Response) {
    try {
      const { id } = req.params;
      const ge = await db.query("SELECT * FROM user WHERE id = ?", [id], (err, rows) => {
        if (!err) {
          res.setHeader('Content-Type', 'application/json');
          res.status(200).send(rows);
          console.log(JSON.stringify(rows));
        } else {
          res.status(500).send(err.sqlMessage);
          console.log(err);
        }
      });
    } catch (error) {
      console.log(error);
    }
  }

  public async create(req: Request, res: Response) {
    try {
      const nueva = req.body;
      await db.query("INSERT INTO user set ?", [req.body], (err) => {
        if (!err) {
          res.setHeader('Content-Type', 'application/json');
          res.status(200).send({ message: "Persona creada", nueva });
          console.log(nueva);
        } else {
          //res.json({error: "No se pudo crear la persona", err});
          res.status(500).send(err.sqlMessage);
          console.log(err.sqlMessage);
        }
      });
      // res.json({massage: 'Persona creada'});
    } catch (error) {
      console.log(error);
    }
  }

  public async update(req: Request, res: Response) {
    try {
      const { id } = req.params;
      await db.query("UPDATE user set ? WHERE id = ?", [req.body, id],(err) => {
        if (!err) {
          res.setHeader('Content-Type', 'application/json');
          res.status(200).send({ message: "Persona actualizada" });
          //res.json({message:req.body});
          console.log("Persona", id, "se ha actualizado");
        } else {
          res.status(500).send({ Error: err });
          console.log(err);
          }
      });
    } catch (error) {
      console.log(error);
    }
  }

  public async delete(req: Request, res: Response) {
    try {
      const { id } = req.params;
      await db.query("DELETE FROM user WHERE id = ?", [id], (err) => {
        if (!err) {
          res.setHeader('Content-Type', 'application/json');
          res.status(200).send({ message: `Persona eliminada con el id '${id}'` });
          console.log("Persona con el id", id, "ha sido eliminada");
        } else {
          res.status(500).send({ Error: err });
          console.log(err);
        }
      });
      // res.json({ massage: "Producto Eliminado", id });
    } catch (error) {
      console.log(error);
    }
  }
}

const userController = new UserController();
export default userController;

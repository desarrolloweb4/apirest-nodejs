import { Router } from 'express';

import messageController from '../controllers/messageController';

class MessageRoutes {
    public router: Router = Router();
    constructor() {
        this.config();
    }

    config(): void{
        this.router.get("/", messageController.getAll);
        this.router.get("/:id", messageController.get);
        this.router.post("/", messageController.create);
        this.router.put("/:id", messageController.update);
        this.router.delete("/:id", messageController.delete);
    }

}

const messageRoutes = new MessageRoutes();
export default messageRoutes.router;
import { Router } from 'express';

import publicacionController from '../controllers/publicationController';

class PublicacionRoutes {
    public router: Router = Router();
    constructor() {
        this.config();
    }

    config(): void{
        this.router.get("/", publicacionController.getAll);
        this.router.get("/:id", publicacionController.get);
        this.router.post("/", publicacionController.create);
        this.router.put("/:id", publicacionController.update);
        this.router.delete("/:id", publicacionController.delete);
    }

}

const publicacionRoutes = new PublicacionRoutes();
export default publicacionRoutes.router;
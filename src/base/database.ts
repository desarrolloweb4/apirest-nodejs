import mysql from 'mysql';
import config from './config';
//import showBanner from 'node-banner';

const connect = mysql.createPool(config.database);

connect.getConnection((err, connection) => {
    try {
        connection.release();
        //showBanner('','Connection success');
        console.log(" Connection To Mysql Success");
    } catch {
        console.log(err);
    }
});

export default connect;
